# syntax=docker/dockerfile:experimental
## 使用 DOCKER_BUILDKIT=1 docker build -t 'imageName:tag' . 的方式進行打包
# 下載平台 apigateway（可討論是否由後端人員交付一個版本包含平台與maven的 base image）

ARG	    apiVersion=3.1.0.1000
ARG	    skipTests=true
ARG	    platformPath=/platform
ARG	    backendName=devom_backend
ARG	    packpath=/package
FROM        alpine AS base
# 由此設定相關參數
ARG	    apiVersion
ARG	    platformPath
ARG	    backendName
ENV         nexusServer=10.40.41.220:8081
WORKDIR     ${platformPath}
RUN         apk add wget --no-cache
RUN         wget -nv 'http://'${nexusServer}'/nexus/service/local/artifact/maven/redirect?r=releases&g=com.digiwin&a=dwapiplatform-appbackend&e=war&v='${apiVersion} -O ${backendName}.war \
            && tar xvf ${backendName}.war \
            && mv app_backend ${backendName}
# 編譯
FROM        maven:3.5.3-jdk-8 AS builder
ARG	    apiVersion
ARG	    skipTests
ARG	    packpath
ARG	    backendName
ENV         build=/build modulePath=develop/module thirdParty=develop/DWThirdPartyLibrary
RUN         mkdir -p ${build}/thirdParty && mkdir -p ${packpath}/lib && mkdir -p ${packpath}/module
RUN	    mkdir -p ${packpath}/conf && mkdir -p ${packpath}/lang  && mkdir -p ${packpath}/lib
WORKDIR     ${build}
# config
COPY        ${backendName}/develop/conf/* ${packpath}/conf/
COPY        ${backendName}/develop/lang/* ${packpath}/lang/
COPY        ${backendName}/develop/lib/* ${packpath}/lib/
# thirdparty
COPY        ${backendName}/${thirdParty} ${thirdParty}
RUN         --mount=type=cache,target=/root/.m2 mvn -f ${thirdParty}/pom.xml package
RUN         cp -r ${thirdParty}/target/dependency/* ${packpath}/lib

# module1
ENV         moduleName=DWDemoAPP
COPY        ${backendName}/${modulePath}/${moduleName}  ${moduleName}
RUN         --mount=type=cache,target=/root/.m2 mvn -f ${moduleName}/pom.xml package
RUN         cp -r ${moduleName}/target/* ${packpath}/module

# 打包
FROM        registry.digiwincloud.com/base/digiwin_openjdk8-alpine:1.0.1.2
ARG	    platformPath
ARG	    backendName
ARG	    packpath
RUN         rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
COPY        --from=base ${platformPath}/${backendName} /${backendName}
WORKDIR     /${backendName}
COPY        --from=builder ${packpath}/ ./application/

RUN         chmod +x ./platform/bin/run.sh \
            && chmod +x ./platform/bin/stop.sh \
            && chmod +x ./platform/bin/docker/dockerEnv.sh \
            && chmod +x ./platform/bin/docker/dockerEnvReplace.sh \
            && chmod +x ./platform/bin/docker/dockerRun.sh
EXPOSE      22620
ENTRYPOINT  ["/devom_backend/platform/bin/docker/dockerRun.sh"]
