package com.digiwin.demo.dwdemo.service;

import com.digiwin.app.service.DWService;

/**
 * @author Miko
 */
public interface IDemoService extends DWService {
    //回傳Hello Application
    public Object get() throws Exception;
}
